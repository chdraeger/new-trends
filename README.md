# Seminar New Trends in Digital Humanities

This repository contains all the research, testing and prototyping I did for my project so far. 
In the seminar we discussed how the Smith–Waterman algorithm can be used to find text-reuse in historical texts. We used a python implementation for the Christus Patiens as an example. Said python implementation will provide the baseline for my project.
The Smith–Waterman algorithm finds a maximized local sequence alignment and is used in bioinformatics to find similar regions in protein sequences. Given two texts we can use the same algorithm to find similar regions of characters in both texts. To find such regions words were compared character-wise.
The goal of this project is to change the exact character-based match into a more granular similarity measure between words provided by their similarity in an word embedding space.

Such an algorithm would then find similar sequences of (word)vectors. That is especially interesting because there are various methods to construct a vector representation of a sentence from its word vectors. Using the Smith–Waterman algorithm on those sentence vectors provides sequences of similar sentences, which then can be used to judge document level similarity. 

# Baseline

This Jupyter Notebook 
https://colab.research.google.com/github/nicoschmidt/DigitalCentones/blob/master/SmartTexts2019/Practicum.ipynb#scrollTo=1je_UIgTFDSg
and the code from 
https://github.com/nicoschmidt/DigitalCentones
provide the baseline implementation.

# Embeddings

The text used in the baseline is ancient greek. As I don't know of any pre-trained embeddings for ancient greek, further work will be done on English text. 
As there are many pre-trained embeddings for English I did some preliminary tests on two popular embeddings.

- https://nlp.stanford.edu/projects/glove/ Glove embedding (i used glove.6B.zip)
- https://code.google.com/archive/p/word2vec/ Word2Vec (i used GoogleNews-vectors-negative300.bin.gz)

Note that the embeddings are too big to save them in this repository.

# R

I worked with word embeddings in R before, so I did some first tests in R with the Glove embedding. I showed the results in the seminar. The examples show how word embeddings can be used to measure the similarity of words. It also includes some visualization of the embedding space to highlight other useful aspects of word embeddings.

The examples also contain an implementation that constructs a vector representation of a sentence from its word vectors. The sentence vector is constructed as the centroid of all word vectors ( add all word vectors, then normalize by the amount of words). The first test of comparing different sentences by their centroid shows that stopwords have a lot of impact on the similarity and that this approach performs worse on longer sentences.
Weighting each word vector before adding it to the centroid might improve the results.

see R/gloveExample.nb.html for results. 

# Python

I reproduced the R example in Python using the Word2Vec embedding. 
The only noticeable result here is that there seems to be more libraries supporting Word2Vec in Python so I will be using that embedding for this project.

# Sentence Level Similarity

While the base approach for this project does not need a sentence level vector representation it seems interesting to see if we can use the same algorithm to find a sequence of similar sentences between two different documents. If we construct a Vector representation of each sentence within the embedding space we can even use the exact same algorithm that was used on the word vectors. 

I had worked with word embeddings before, mainly to classify short text (search snippets). The centroid representation for a sentence or even document works very well for that task. (see 7.) However, this task is more about text reuse/paraphrase detection. For these tasks weighting each word vector before building the centroid seems beneficial. Different weighting methods are discussed in 4. and a simple TF-IDF weight seems to perform well.
 
# Document Level Similarity

It is possible to construct a centroid vector for each document just as described for the sentence Level Similarity. However, this performs poorly on long Documents as we lose all information about word order when constructing the centroid. A better way to judge Document Similarity is to use topic analysis to find documents with a similar sequence of topics (see 6.). 
With the proposed algorithm for sentence-level similarity we could compare documents in a similar way, by directly comparing sequences of similar sentences. 

This goes beyond the scope of this project for now but it is a very interesting application, as it enables us to use the same algorithm to compare words, sentences, and documents.

# Testing

Different Datasets for Paraphrase Detection. I would mostly test on the MSRP corpus. It is lightweight, and the paper that seems to follow a very similar approach (4.) also evaluates on it.

- https://github.com/cocoxu/SemEval-PIT2015    
SemEval-2015 (twitter test data)

- https://lanwuwei.github.io/Twitter-URL-Corpus/
Language-Net: The Large Scale Paraphrase Dataset

- https://www.microsoft.com/en-us/download/details.aspx?id=52398
Microsoft Research Paraphrase Corpus (MSRP)

# Implementation

Unfortunately, I could not complete the implementation for this project. 
The main task for this project is to change the Smith–Waterman algorithm from the baseline to also work on word embeddings. When starting this project I assumed I could change the similarity measure within the algorithm to achieve just that. 
It turned out that it does not work and it would be better practice to re-implement the whole algorithm for word embeddings.

# Visualisation

The Visualisation in the baseline implementation is handled by outputting an html file. I assume the main reason for this approach is to visualize the output within the Jupyter Notebook. 
If we have a larger body of test data reviewing it like that would be rather subjective. However, seeing some of the aligned results might be helpful. 

In general, I think it is best to not implement the visualization within this project. The general assumption for this project is that we get two text documents, most likely as an xml (or csv, .json etc.). We could then return the result in a file of the same type. Containing the information which part of the documents are similar. The assumption here is that the source of the two documents would already have a way of visualizing them. It is then better to just provide the information about what to highlight rather than displaying the result elsewhere.
As a short side project, it might be beneficial to write a script that takes both documents and the results to produce a highlighted version in html (or in any other markup language).

In total, I think it is best to just save the results of the algorithm in form of a .json/csv/xml. That is also good enough to evaluate the results on the test data.  
# Future Work

As the implementational work for this project is not done, the proposed variation on the Smith–Waterman algorithm needs to be implemented first. 
To me, the motivation behind this project is not to outperform existing methods of detecting text reuse (and I would be surprised if it did). What I find interesting about this approach is that it gives multiple ways to adjust the desired level of similarity (both within the Smith–Waterman algorithm itself and with the similarity measure of the embedding). Giving that control the user of a digital reading environment makes for a great tool for explorative search. 

Integrating this algorithm into a digital reading environment would give a good opportunity to test if this approach can be used to measure document level similarity. 
# Research

1. http://proceedings.mlr.press/v37/kusnerb15.pdf
  Document Similarity with Word Mover Distance
  
2. http://www.qizhang.info/paper/duplicate.sigir2014.pdf
  word embeddings to detect text reuse
  
3. https://arxiv.org/pdf/1712.02820.pdf
  text reuse with neural nets

4. https://www.aclweb.org/anthology/N15-1154.pdf
  Phrase Embedding, on MSRP corpus, discusses different weighting methods for phrases (tfid seems good enough)
  
5. https://github.com/erikreppel/paraphrase-detection
  Paraphrase Detection, implementation in Python
  
6. https://arxiv.org/pdf/1611.04822.pdf
  SimDoc, Document Similarity based on Topic Sequence Alignment
  
7. https://www.aclweb.org/anthology/E17-2072.pdf
  centroid preforms for text classification
