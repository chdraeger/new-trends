# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 22:32:33 2019

@author: VictoryThroughUnity
"""

from gensim.models import KeyedVectors
# Load vectors directly from the file
model = KeyedVectors.load_word2vec_format('data/GoogleNews-vectors-negative300.bin', binary=True)
# Access vectors for specific words with a keyed lookup:
vector = model['easy']
# see the shape of the vector (300,)
vector.shape
# Processing sentences is not as simple as with Spacy:
vectors = [model[x] for x in "This is some text I am processing with Spacy".split(' ')]
#pretrained model is read in

print("Goodbye, World!")

model.similarity('easy','simple')
model.similarity('orange','apple')
model.similarity('juice','apple')
model.most_similar('juice')

textA = 'Megabats constitute the family Pteropodidae of the order Chiroptera (bats). They are also called fruit bats, Old World fruit bats,[2] or, especially the genera Acerodon and Pteropus, flying foxes. The evolution of megabats has been determined primarily by genetic data, as the fossil record for this family is the worst of all bats. Megabats likely evolved in Australasia, with the common ancestor of all living pteropodids existing approximately 31 Ma (million years ago). Many megabat lineages likely originated in Melanesia, then dispersed to mainland Asia, the Mediterranean, and Africa over time. Megabats are found in tropical and subtropical areas of Eurasia, Africa, and Oceania.[3][4][5] Compared to insectivorous bats, fruit bats are relatively large, and with some exceptions, do not navigate by echolocation. Most species are primarily frugivores and rely on their keen senses of sight and smell to locate food.[6] They reach sexual maturity slowly, and have a low reproductive output. Most species have one offspring at a time after a pregnancy of 4–6 months. This low reproductive output means that, after a population loss, their populations are slow to rebound, making them more susceptible to threats. A quarter of all megabat species are listed as threatened, with key responsible factors as habitat destruction and overhunting. Megabats are a popular food source in some areas, leading to population declines and extinction. While megabats can be a useful food resource, they are also of interest to public health as the natural reservoirs of several viruses than can affect humans.'
textB = 'Megabats constitute the family Pteropodidae of the order Chiroptera (bats). They are also called fruit bats, Old World fruit bats, or—especially the genera Acerodon and Pteropus—flying foxes. They are the only member of the superfamily Pteropodoidea, which is one of two superfamilies in the suborder Yinpterochiroptera. Internal divisions of Pteropodidae have varied since subfamilies were first proposed in 1917. From three subfamilies in the 1917 classification, six are now recognized, along with various tribes. As of 2018, 197 species of megabat had been described. The evolution of megabats has been determined primarily by genetic data, as the fossil record for this family is the most fragmented of all bats. They likely evolved in Australasia, with the common ancestor of all living pteropodids existing approximately 31 million years ago. Many of their lineages probably originated in Melanesia, then dispersed over time to mainland Asia, the Mediterranean, and Africa. Today, they are found in tropical and subtropical areas of Eurasia, Africa, and Oceania. The megabat family contains the largest bat species, with individuals of some species weighing up to 1.45 kg (3.2 lb) and having wingspans up to 1.7 m (5.6 ft). Not all megabats are large-bodied; nearly a third of all species weigh less than 50 g (1.8 oz). They can be differentiated from other bats due to their dog-like faces, clawed second digits, and reduced uropatagium. Only members of one genus, Notopteris, have tails. Megabats have several adaptations for flight, including rapid oxygen consumption, the ability to sustain heart rates of more than 700 beats per minute, and large lung volumes. Most megabats are nocturnal or crepuscular, but a few species are active during the day-time. During the period of inactivity, they roost in trees or caves. Some species roost alone, while others form colonies up to a million individuals. During the period of activity, they use flight to travel to food resources. With few exceptions, they are unable to echolocate, relying instead on their keen senses of sight and smell to navigate and locate food. Most species are primarily frugivorous and several are nectarivorous. Other less common food resources include leaves, pollen, twigs, and bark. They reach sexual maturity slowly and have a low reproductive output. Most species have one offspring at a time after a pregnancy of four to six months. This low reproductive output means that after a population loss their numbers are slow to rebound. A quarter of all species are listed as threatened, mainly due to habitat destruction and overhunting. Megabats are a popular food source in some areas, leading to population declines and extinction. While they can be a useful food resource, they are also of interest to those involved in public health; they are the natural reservoirs of several viruses that can affect humans. '

from DigitalCentones.smith_waterman import SmithWaterman
from DigitalCentones.alignment import *
from IPython.display import HTML
import numpy as np

smith_waterman = SmithWaterman(match_score=2,
                               mismatch_score=-1,
                               gap_score=-1,
                               n_max_alignments=1,
                               min_score_treshold=0)
alignments_outfile = 'megabat'
target_lines = textB
source_lines_dict = textA
alignments = find_line_to_line_alignments(target_lines, source_lines_dict, smith_waterman.align, 3, lambda line:line.split(' '), line_to_line_map)
save_alignments_html(alignments, target_lines, source_lines_dict, alignments_outfile, lambda line:line.text_raw.split(' '), 4)


lines_chr_pat[1].text_no_diacritics.split(' ')
{lines_medea[0].work_id:lines_medea}





